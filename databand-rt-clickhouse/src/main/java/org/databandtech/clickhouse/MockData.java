package org.databandtech.clickhouse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import org.databandtech.clickhouse.entity.EventLog;
import org.databandtech.clickhouse.utils.Mock;

import ru.yandex.clickhouse.ClickHouseConnection;
import ru.yandex.clickhouse.ClickHouseDataSource;
import ru.yandex.clickhouse.ClickHouseStatement;
import ru.yandex.clickhouse.settings.ClickHouseProperties;
import ru.yandex.clickhouse.settings.ClickHouseQueryParam;

/*
 * CREATE TABLE default.SNM_initial20210916 \
( \
    `EventTime` DateTime, \
    `EventDate` Date, \
    `IP` String, \
    `Uid` UInt32, \
    `Cid` String, \
    `Ctitle` String, \
    `Vid` String, \
    `Vtitle` String, \
    `Source` String, \
    `Logtype` String, \
    `City` String, \
    `Duration` UInt16, \
    `Pageid` UInt8 \
) \
ENGINE = MergeTree() \
PARTITION BY toYYYYMM(EventDate) \
ORDER BY (EventDate, intHash32(Uid)) \
SAMPLE BY intHash32(Uid);

SELECT
    table AS `表名`,
    sum(rows) AS `总行数`,
    formatReadableSize(sum(data_uncompressed_bytes)) AS `原始大小`,
    formatReadableSize(sum(data_compressed_bytes)) AS `压缩大小`,
    round((sum(data_compressed_bytes) / sum(data_uncompressed_bytes)) * 100, 0) AS `压缩率`
FROM system.parts
GROUP BY table;

 */
public class MockData {
	
	final static String[] CITYS = {"北京","北京","北京","上海","上海","上海","广州","广州","深圳","深圳","重庆","杭州","武汉","南京","郑州","西安","成都","长沙"};
	final static String[] APPS = {"jg","mtv"};
	final static int COUNT=10000;    //发送的数据条数
	final static int PARTITION=0; //分区
	final static String DATE="2021-09-18 "; //日志日期
	final static int MAXUUSERID=10000; //Uid范围
	final static int MAXVID=3000; //Vid范围
	
	public static void main(String[] args) {

		String url = Config.URL;
		ClickHouseProperties properties = new ClickHouseProperties();
		// set connection options - see more defined in ClickHouseConnectionSettings
		properties.setClientName("Agent #1");
		// set default request options - more in ClickHouseQueryParam
		properties.setSessionId("default-session-id");

		ClickHouseDataSource dataSource = new ClickHouseDataSource(url, properties);
		String sql = "select EventTime,Uid,Vid,Source,Logtype,City,Duration from SNM_initial20210916 order by EventTime desc limit 10";

		Map<ClickHouseQueryParam, String> additionalDBParams = new HashMap<ClickHouseQueryParam, String>();
		// set request options, which will override the default ones in
		// ClickHouseProperties
		additionalDBParams.put(ClickHouseQueryParam.SESSION_ID, "new-session-id");

		ClickHouseConnection conn = null;
		try {
			conn = dataSource.getConnection();	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			ClickHouseStatement stmt = conn.createStatement();
			for (int i = 1; i <= COUNT; i++) {
				EventLog log = new EventLog(DATE+Mock.getNum(10, 23)+":"+Mock.getNum(10, 30)+":00", 
						Mock.getNum(0, MAXUUSERID), Mock.getNum(0, MAXVID)+"", APPS[Mock.getNum(0, APPS.length-1)], "vh", CITYS[Mock.getNum(0, CITYS.length-1)], Mock.getNum(0, 500));
				
				String insert_sql = "INSERT INTO SNM_initial20210916(EventTime,Uid,Vid,Source,Logtype,City,Duration) "
						+ "VALUES('"+log.getEventtime()+"', "+ log.getUid() +",'"+ log.getVid() +"','"+ 
						log.getSource()+"','"+log.getLogtype() +"','"+log.getCity() +"',"+log.getDuration() +")";
				
				//单条执行非常慢，弃用
				//stmt.execute(insert_sql);
				stmt.addBatch(insert_sql);
			}
			stmt.executeBatch();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}


		// 执行查询
    	try (
    		ClickHouseStatement	stmt = conn.createStatement();
    	    ResultSet rs = stmt.executeQuery(sql, additionalDBParams)) {
    		while (rs.next()) {
				System.out.println(rs.getString("EventTime")+"-"+
						rs.getInt("Uid")+" / "+
						rs.getString("Vid")+" / "+
						rs.getString("Source")+" / "+
						rs.getString("Logtype")+" / "+
						rs.getString("City")+" / "+
						rs.getInt("Duration"));
			}

    	} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
