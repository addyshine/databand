package org.databandtech.mockmq.entity;

import java.io.Serializable;

public class EpgVod implements Serializable{
	

	private static final long serialVersionUID = 5089940991153421803L;
	String action_type;
	String sys_id;
	String user_id;
	String user_group_id;
	String epg_group_id;
	String stb_ip;
	String stb_id;
	String stb_type;
	String stb_mac;
	String terminal_type;
	String log_time;
	String mediacode;
	String definition;
	String bitrate;
	String start_time;
	String currentplaytime;
	String refer_type;
	String refer_page_id;
	String area_code; 

	public String getAction_type() {
		return action_type;
	}
	public void setAction_type(String action_type) {
		this.action_type = action_type;
	}
	public String getSys_id() {
		return sys_id;
	}
	public void setSys_id(String sys_id) {
		this.sys_id = sys_id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_group_id() {
		return user_group_id;
	}
	public void setUser_group_id(String user_group_id) {
		this.user_group_id = user_group_id;
	}
	public String getEpg_group_id() {
		return epg_group_id;
	}
	public void setEpg_group_id(String epg_group_id) {
		this.epg_group_id = epg_group_id;
	}
	public String getStb_ip() {
		return stb_ip;
	}
	public void setStb_ip(String stb_ip) {
		this.stb_ip = stb_ip;
	}
	public String getStb_id() {
		return stb_id;
	}
	public void setStb_id(String stb_id) {
		this.stb_id = stb_id;
	}
	public String getStb_type() {
		return stb_type;
	}
	public void setStb_type(String stb_type) {
		this.stb_type = stb_type;
	}
	public String getStb_mac() {
		return stb_mac;
	}
	public void setStb_mac(String stb_mac) {
		this.stb_mac = stb_mac;
	}
	public String getTerminal_type() {
		return terminal_type;
	}
	public void setTerminal_type(String terminal_type) {
		this.terminal_type = terminal_type;
	}
	public String getLog_time() {
		return log_time;
	}
	public void setLog_time(String log_time) {
		this.log_time = log_time;
	}
	public String getMediacode() {
		return mediacode;
	}
	public void setMediacode(String mediacode) {
		this.mediacode = mediacode;
	}
	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	public String getBitrate() {
		return bitrate;
	}
	public void setBitrate(String bitrate) {
		this.bitrate = bitrate;
	}
	public String getStart_time() {
		return start_time;
	}
	public void setStart_time(String start_time) {
		this.start_time = start_time;
	}
	public String getCurrentplaytime() {
		return currentplaytime;
	}
	public void setCurrentplaytime(String currentplaytime) {
		this.currentplaytime = currentplaytime;
	}
	public String getRefer_type() {
		return refer_type;
	}
	public void setRefer_type(String refer_type) {
		this.refer_type = refer_type;
	}
	public String getRefer_page_id() {
		return refer_page_id;
	}
	public void setRefer_page_id(String refer_page_id) {
		this.refer_page_id = refer_page_id;
	}
	public String getArea_code() {
		return area_code;
	}
	public void setArea_code(String area_code) {
		this.area_code = area_code;
	}
}
