package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 动态单对象 dyform
 * 
 * @author ruoyi
 * @date 2021-06-11
 */
public class Dyform extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private Long id;

    /** 表单名称 */
    @Excel(name = "表单名称")
    private String name;

    /** $column.columnComment */
    @Excel(name = "表单名称")
    private String jsonstr;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setJsonstr(String jsonstr) 
    {
        this.jsonstr = jsonstr;
    }

    public String getJsonstr() 
    {
        return jsonstr;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("jsonstr", getJsonstr())
            .toString();
    }
}
