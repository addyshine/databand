import { getDicts,getDictsSync} from "@/api/system/dict/data";

export default {
  options(h, conf, key) {
    if (conf.__config__.ryType === 'dictcheckbox') //若依字典列表
    {
      const list = []
      let data = JSON.parse(getDictsSync(conf.__slot__[key]))
      let opts = data.data
        opts.forEach(item => {
          if (conf.__config__.optionType === 'button') {
            list.push(<el-checkbox-button label={item.dictValue}>{item.dictLabel}</el-checkbox-button>)
          } else {
            list.push(<el-checkbox label={item.dictValue} border={conf.border}>{item.dictLabel}</el-checkbox>)
          }
        })
        return list
      }  
    else {//普通列表
    const list = []
    conf.__slot__.options.forEach(item => {
      if (conf.__config__.optionType === 'button') {
        list.push(<el-checkbox-button label={item.value}>{item.label}</el-checkbox-button>)
      } else {
        list.push(<el-checkbox label={item.value} border={conf.border}>{item.label}</el-checkbox>)
      }
    })
    return list
  }
  }
}
