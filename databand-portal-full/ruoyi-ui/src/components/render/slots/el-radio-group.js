import { getDicts,getDictsSync} from "@/api/system/dict/data";

export default {
  options(h, conf, key) {
    if (conf.__config__.ryType === 'dictradio') //若依字典列表
    {
      const list = []
      let data = JSON.parse(getDictsSync(conf.__slot__[key]))
      let opts = data.data
        opts.forEach(item => {
          if (conf.__config__.optionType === 'button') {
            list.push(<el-radio-button label={item.dictValue}>{item.dictLabel}</el-radio-button>)
          } else {
            list.push(<el-radio label={item.dictValue} border={conf.border}>{item.dictLabel}</el-radio>)
          }
        })
        return list
      }  
    else {//普通列表
    const list = []
    conf.__slot__.options.forEach(item => {
      if (conf.__config__.optionType === 'button') {
        list.push(<el-radio-button label={item.value}>{item.label}</el-radio-button>)
      } else {
        list.push(<el-radio label={item.value} border={conf.border}>{item.label}</el-radio>)
      }
    })
    return list
  }
  }
}
