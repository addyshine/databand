package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Dyform;
import com.ruoyi.system.service.IDyformService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 动态单Controller
 * 
 * @author ruoyi
 * @date 2021-06-11
 */
@RestController
@RequestMapping("/system/dyform")
public class DyformController extends BaseController
{
    @Autowired
    private IDyformService dyformService;

    /**
     * 查询动态单列表
     */
    @PreAuthorize("@ss.hasPermi('system:dyform:list')")
    @GetMapping("/list")
    public TableDataInfo list(Dyform dyform)
    {
        startPage();
        List<Dyform> list = dyformService.selectDyformList(dyform);
        return getDataTable(list);
    }

    /**
     * 导出动态单列表
     */
    @PreAuthorize("@ss.hasPermi('system:dyform:export')")
    @Log(title = "动态单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Dyform dyform)
    {
        List<Dyform> list = dyformService.selectDyformList(dyform);
        ExcelUtil<Dyform> util = new ExcelUtil<Dyform>(Dyform.class);
        return util.exportExcel(list, "dyform");
    }

    /**
     * 获取动态单详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:dyform:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dyformService.selectDyformById(id));
    }

    /**
     * 新增动态单
     */
    @PreAuthorize("@ss.hasPermi('system:dyform:add')")
    @Log(title = "动态单", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Dyform dyform)
    {
        return toAjax(dyformService.insertDyform(dyform));
    }

    /**
     * 修改动态单
     */
    @PreAuthorize("@ss.hasPermi('system:dyform:edit')")
    @Log(title = "动态单", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Dyform dyform)
    {
        return toAjax(dyformService.updateDyform(dyform));
    }

    /**
     * 删除动态单
     */
    @PreAuthorize("@ss.hasPermi('system:dyform:remove')")
    @Log(title = "动态单", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dyformService.deleteDyformByIds(ids));
    }
}
