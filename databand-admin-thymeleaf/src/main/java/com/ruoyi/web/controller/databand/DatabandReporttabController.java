package com.ruoyi.web.controller.databand;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.web.domain.DatabandReporttab;
import com.ruoyi.web.service.IDatabandReportService;
import com.ruoyi.web.service.IDatabandReporttabService;
import com.ruoyi.web.service.IDatabandSourceService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 报页Controller
 * 
 * @author databand
 * @date 2020-12-31
 */
@Controller
@RequestMapping("/web/reporttab")
public class DatabandReporttabController extends BaseController
{
    private String prefix = "web/reporttab";

    @Autowired
    private IDatabandReporttabService databandReporttabService;
    @Autowired
    private IDatabandSourceService databandSourceService;
    @Autowired
    private IDatabandReportService databandReportService;
    
    
    @RequiresPermissions("web:reporttab:view")
    @GetMapping()
    public String reporttab()
    {
        return prefix + "/reporttab";
    }

    /**
     * 查询报页列表
     */
    @RequiresPermissions("web:reporttab:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DatabandReporttab databandReporttab)
    {
        startPage();
        List<DatabandReporttab> list = databandReporttabService.selectDatabandReporttabList(databandReporttab);
        return getDataTable(list);
    }

    /**
     * 导出报页列表
     */
    @RequiresPermissions("web:reporttab:export")
    @Log(title = "报页", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(DatabandReporttab databandReporttab)
    {
        List<DatabandReporttab> list = databandReporttabService.selectDatabandReporttabList(databandReporttab);
        ExcelUtil<DatabandReporttab> util = new ExcelUtil<DatabandReporttab>(DatabandReporttab.class);
        return util.exportExcel(list, "reporttab");
    }

    /**
     * 新增报页
     */
    @GetMapping("/add")
    public String add(ModelMap mmap)
    {
    	mmap.put("datasource", databandSourceService.selectDatabandSourceList());
    	mmap.put("report", databandReportService.selectDatabandReportList());
    	
        return prefix + "/add";
    }

    /**
     * 新增保存报页
     */
    @RequiresPermissions("web:reporttab:add")
    @Log(title = "报页", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DatabandReporttab databandReporttab)
    {
        return toAjax(databandReporttabService.insertDatabandReporttab(databandReporttab));
    }

    /**
     * 修改报页
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        DatabandReporttab databandReporttab = databandReporttabService.selectDatabandReporttabById(id);
        mmap.put("databandReporttab", databandReporttab);
    	mmap.put("datasource", databandSourceService.selectDatabandSourceList());
    	mmap.put("report", databandReportService.selectDatabandReportList());

        return prefix + "/edit";
    }

    /**
     * 修改保存报页
     */
    @RequiresPermissions("web:reporttab:edit")
    @Log(title = "报页", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DatabandReporttab databandReporttab)
    {
        return toAjax(databandReporttabService.updateDatabandReporttab(databandReporttab));
    }

    /**
     * 删除报页
     */
    @RequiresPermissions("web:reporttab:remove")
    @Log(title = "报页", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(databandReporttabService.deleteDatabandReporttabByIds(ids));
    }
}
