package org.databandtech.logmock;

import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.databandtech.common.Mock;
import org.databandtech.logmock.entity.Person;
import org.databandtech.logmock.utils.Csv;

/**
 * Csv生成简单demo,csv文件生成，运行后在"FILE_PATH"定义的文件夹中可找到csv文件
 * @author wangixn
 *
 */
public class CsvMock {
	
	static int COUNT = 1000;
	static int AGE_LOW = 20;
	static int AGE_HIGH = 100;
	static String FILE_PATH = "c:\\logs\\csv\\"+Instant.now().getEpochSecond()+".csv";

	public static void main(String[] args) throws Exception {
		List<Person> persons = new ArrayList<Person>();
		for(int i=0;i<COUNT;i++) {
			Person p = new Person();
			p.setAddress(Mock.getRoad());
			p.setAge(Mock.getNum(AGE_LOW,AGE_HIGH));
			int sex = Mock.getNum(0, 1);
			p.setName(Mock.getChineseName());
			p.setPhone(Mock.getTel());
			p.setSex(sex);
			p.setDate(LocalDate.of(2020, Mock.getNum(1, 10), Mock.getNum(1, 30)).toString());
			persons.add(p);
		}
	
		Csv.writeCSV(persons, FILE_PATH);
	}

}
