import request from '@/utils/request'

// export function getDatasetDataset() {
//   return request({
//     url: '/chart/dataset/dataset',
//     method: 'get'
//   })
// }

export function getDatasetByType(datetype, producttype, channel, from, to) {
  return request({
    url: '/chart/dataset/datasetbytype',
    method: 'get',
    params: { datetype, producttype, channel, from, to }
  })
}

export function getDatasetSeries() {
  return request({
    url: '/chart/dataset/series',
    method: 'get'
  })
}

export function getPieSeries(datetype) {
  return request({
    url: '/chart/pie/series',
    method: 'get',
    params: { datetype }
  })
}

export function getRadarData() {
  return request({
    url: '/chart/radar/data',
    method: 'get'
  })
}

