package org.databandtech.mockapi;

import java.util.Date;
import java.util.List;

import org.databandtech.mockapi.entity.MockInstance;

/**
 * 用于装载新mock实例,如果数据库记录有增加（通过maxid比对），则加入新规则
 * @author Administrator
 *
 */
public class MockAppendTask implements Runnable {
	
    public MockAppendTask() {
        System.out.println("MockAppendTask startTime : " + new Date());
    }

    public void run() {   	
        List<MockInstance> newInstances = App.getDataFromDb(App.MAXID);
        App.mockInstancesAssembling(newInstances);        
        for (MockInstance ins : newInstances) {
            System.out.println("new instance is loaded : " + ins.getDescribe() + ", startTime = " + new Date());        	
        }
    }
    
}