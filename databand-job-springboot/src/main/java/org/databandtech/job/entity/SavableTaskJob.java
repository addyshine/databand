package org.databandtech.job.entity;

import java.sql.ResultSet;

import org.databandtech.job.sink.HiveSqlQueryJob1MySQLSink;
import org.databandtech.job.sink.SinkFunction;

public interface SavableTaskJob<T> {
	
	//String SaveData(T t, HiveSqlQueryJob1MySQLSink dest);
	String SaveData(ResultSet rs, SinkFunction sink); 

}
