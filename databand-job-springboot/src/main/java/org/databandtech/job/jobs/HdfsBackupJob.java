package org.databandtech.job.jobs;

import org.databandtech.job.entity.ScheduledTaskJob;
import org.databandtech.job.utils.HdfsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HdfsBackupJob implements ScheduledTaskJob{
	
	public HdfsBackupJob(String key,  String localPath, String hdfsPath ,String cron) {
		super();
		this.localPath = localPath;
		this.hdfsPath = hdfsPath;
		this.key = key;
		this.cron = cron;
	}
	private String key;	
	private String cron;
	private String localPath; 
	private String hdfsPath;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HdfsBackupJob.class);
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
	
	public String getCron() {
		return cron;
	}

	public void setCron(String cron) {
		this.cron = cron;
	}
	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}

	public String getHdfsPath() {
		return hdfsPath;
	}

	public void setHdfsPath(String hdfsPath) {
		this.hdfsPath = hdfsPath;
	}
	
	public void main(String[] args) {
		//第一个参数true:删除源文件，即本地文件
		//第二个参数true:覆盖hdfs原有文件。 false：不overwrite,如果存在则会抛异常
		HdfsUtils.copyFromLocalFile(true,true,localPath, hdfsPath);
	}

	@Override
	public void run() {
		LOGGER.info("CommandExecuteJob => {}  run  当前线程名称 {} ", key, Thread.currentThread().getName());
		HdfsUtils.copyFromLocalFile(true,true,localPath, hdfsPath);
	}
	
	     

}
